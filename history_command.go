package main

import (
	"fmt"
	"os"
	"path"
	"strings"
	"time"
)

// HistoryCommand contains fields to be written to the history file
type HistoryCommand struct {
	Command     string
	DatePath    string
	CommandDate string
	Tty         string
	Hostname    string
	Directory   string
}

// NewHistoryCommand returns a HistoryCommand struct containing fields populated with the given parameters
func NewHistoryCommand(args []string) HistoryCommand {
	return HistoryCommand{
		Directory:   args[0],
		Tty:         args[1],
		CommandDate: args[2],
		Command:     args[3],
		DatePath:    time.Now().Format("2006/01/02"),
		Hostname:    getHostname(),
	}
}

// CreateDirectory creates a history directory in the format of $HOME/history/2020/10/10
func (hc *HistoryCommand) CreateDirectory() error {
	path := path.Join(os.Getenv("HOME"), baseDirectory, hc.DatePath)
	err := os.MkdirAll(path, 0755)
	if err != nil {
		return fmt.Errorf("There was an error in CreateDirectory: %s", err.Error())
	}
	return nil
}

// String returns the string representation of the HistoryCommand struct based on the historyFormatString const
func (hc *HistoryCommand) String() string {
	return fmt.Sprintf(historyFormatString, hc.CommandDate, hc.Hostname, hc.Directory, hc.ParseCommand())
}

// ParseCommand parses the zsh command and removes the first entry
func (hc *HistoryCommand) ParseCommand() string {
	parts := strings.Split(hc.Command, ";")
	return strings.Join(parts[1:], ";")
}

// WriteCommand writes the command to the file in the history folder.  The history file is named after the TTY.
func (hc *HistoryCommand) WriteCommand() error {
	filePath := path.Join(os.Getenv("HOME"), baseDirectory, hc.DatePath, fmt.Sprintf("%s.log", hc.Tty))
	f, err := os.OpenFile(filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		return fmt.Errorf("There was an error in WriteCommand when opening the file: %s", err)
	}

	if _, err = f.Write([]byte(hc.String())); err != nil {
		f.Close()
		return fmt.Errorf("There was an error in WriteCommand when writing to the file: %s", err)
	}

	if err = f.Close(); err != nil {
		return fmt.Errorf("There was an error in WriteCommand when closing to the file: %s", err)
	}

	return nil
}

func getHostname() string {
	h, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	return h
}
