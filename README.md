# History Log
This is a simple program that writes the last entry of the zsh shell history to
a file based on TTY.  Use this in cojunction with the `PROMPT_COMMAND` to
activate the history logger.  This program was designed to work with zsh but
should work with bash after minor modification.
## Installation
Build the binary by invoking `go build .` in the directory after cloning the
repository.  Then you can copy it to a directory in your `$PATH`:
```
$ cp history_log /usr/local/bin/history_log
```
## Setup
Add the following lines to your `.zshrc` configuration:
```
PROMPT_COMMAND="/usr/local/bin/write_history"
precmd() { eval "${PROMPT_COMMAND}" }
```
Then, copy the `write_history` shell script to a directory in your shell `$PATH`:
```
$ cp write_history /usr/local/bin/write_history
```
Finally, make it executable:
```
$ chmod +x /usr/local/bin/write_history
```
Starting a new shell should active the history log and write logs
to the `$HOME/history` directory.  The logs will be written to files named after
the TTY of the current shell, e.g:
`$HOME/history/2020/10/12/4.log`

## Why does this exist?
The ZSH history (while configurable) only retains a set number of entries in the
history file.  This program will ensure that I can still search for commands
long after the command expires from that file.  This method also allows
me to lookup a series of commands associated with a given operation (providing
context) for a particular shell instance.
