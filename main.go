package main

import (
	"fmt"
	"os"
)

const baseDirectory = "history"
const historyFormatString = "[%s][%s][%s]: %s\n"

func main() {
	os.Exit(Cli(os.Args))
}

// Cli invokes NewHistoryCommand with the given args and returns an exit code
func Cli(args []string) int {
	hc := NewHistoryCommand(args[1:])
	if err := hc.CreateDirectory(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		return 1
	}

	if err := hc.WriteCommand(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		return 1
	}

	return 0
}
